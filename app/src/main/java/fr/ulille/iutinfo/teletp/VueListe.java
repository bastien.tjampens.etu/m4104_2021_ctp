package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class VueListe extends Fragment /* TODO Q7 */ {

    // TODO Q2c
    SuiviViewModel model;
    // TODO Q6
    SuiviAdapter sa;
    
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_liste, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.btnToGenerale).setOnClickListener(view1 -> NavHostFragment.findNavController(VueListe.this)
                .navigate(R.id.liste_to_generale));

        // TODO Q2c
        model = new SuiviViewModel(getActivity().getApplication());


        // TODO Q6.b
        this.sa = new SuiviAdapter(this.model);

        RecyclerView rvQuestions = (RecyclerView) view.findViewById(R.id.rvQuestions);
        rvQuestions.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvQuestions.setAdapter(this.sa);
        // TODO Q7
        // TODO Q8
    }
}