package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    // TODO Q1
    String salle = this.DISTANCIEL;
    String poste = "";
    String DISTANCIEL;
    // TODO Q2.c
    SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        this.DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
        // TODO Q2.c
        model = new SuiviViewModel(getActivity().getApplication());
        // TODO Q4
        Spinner spinnerSalle = (Spinner) view.findViewById(R.id.spSalle);
        ArrayAdapter<CharSequence> spinnerArrayAdapter1 = ArrayAdapter.createFromResource(this.getContext(), R.array.list_salles,android.R.layout.simple_spinner_item);
        spinnerSalle.setAdapter(spinnerArrayAdapter1);
        spinnerSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Spinner spinnerPoste = (Spinner) view.findViewById(R.id.spPoste);
        ArrayAdapter<CharSequence> spinnerArrayAdapter2 = ArrayAdapter.createFromResource(this.getContext(), R.array.list_postes,android.R.layout.simple_spinner_item);
        spinnerPoste.setAdapter(spinnerArrayAdapter2);
        spinnerPoste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
            TextView tvLogin = (TextView) view.findViewById(R.id.tvLogin);
            this.model.setUsername(tvLogin.getText().toString());
        });

        // TODO Q5.b
        this.update();
        // TODO Q9
    }

    // TODO Q5.a

    void update() {
        Spinner spinnerSalle = (Spinner) this.getView().findViewById(R.id.spSalle);
        Spinner spinnerPoste = (Spinner) this.getView().findViewById(R.id.spPoste);
        if(spinnerSalle.getSelectedItemPosition() == 0) {
            spinnerPoste.setVisibility(View.GONE);
            spinnerPoste.setEnabled(false);
            this.model.setLocalisation(getActivity().getResources().getStringArray(R.array.list_salles)[0]);
            return;
        } else {
            spinnerPoste.setVisibility(View.VISIBLE);
            spinnerPoste.setEnabled(true);
        }
        String[] tmp = getActivity().getResources().getStringArray(R.array.list_salles);
        this.DISTANCIEL = tmp[spinnerSalle.getSelectedItemPosition()];
        System.out.println(this.DISTANCIEL);
        String concat = spinnerSalle.getSelectedItem().toString() + " - "+spinnerPoste.getSelectedItem().toString();
        this.model.setLocalisation(concat);
    }
    // TODO Q9
}